import unittest

if __name__ == "__main__":
    loader = unittest.TestLoader()
    loader.testMethodPrefix = "test"  # default value is "test"
    suite1 = loader.discover('test_movie_api', pattern="test*.py")
    all_tests = unittest.TestSuite((suite1, ))
    unittest.TextTestRunner(verbosity=2).run(all_tests)
