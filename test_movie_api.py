import unittest
from movie_api import tmdb_authentication,\
    rt_authentication, tmdb_movie_search, rt_movie_search,\
    decode_response_from_tmdb, decode_response_from_rt, merge_responses_from_tmdb_and_rt
from movie_api_keys import rotten_tomatoes_key as rt_key
from movie_api_keys import tmdb_key
import re
import urllib2
from flask import jsonify


class MovieAPITesting(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        tmdb_authentication(tmdb_key)
        rt_authentication(rt_key)

    @classmethod
    def tearDownClass(cls):
        pass

    def test_searching_an_existing_movie_through_tmdb(self):
        movie_title = 'the matrix'
        search_response_from_tmdb = tmdb_movie_search(movie_title)
        list_of_identified_original_titles = [result['original_title']
                                              for result in search_response_from_tmdb['results']]
        self.assertGreater(len(search_response_from_tmdb['results']), 0)

    def test_searching_an_existing_movie_through_rt(self):
        movie_title = 'the matrix'
        search_response_from_rt = rt_movie_search(movie_title)
        list_of_identified_original_titles = [result['title']
                                              for result in search_response_from_rt['movies']]
        self.assertGreater(len(search_response_from_rt['movies']), 0)

    def test_searching_an_empty_title_through_tmdb(self):
        movie_title = ''
        search_response_from_tmdb = tmdb_movie_search(movie_title)
        self.assertEqual(search_response_from_tmdb, '422 Client Error: . Please try again later')

    def test_searching_a_non_existing_movie_through_tmdb(self):
        movie_title = 'xzcvzdbcvbxz' # this movie does not exists
        search_response_from_tmdb = tmdb_movie_search(movie_title)
        self.assertEqual(search_response_from_tmdb['results'], [])

    def test_searching_an_empty_title_through_rt(self):
        movie_title = ''
        search_response_from_rt = rt_movie_search(movie_title)
        self.assertFalse('movies' in search_response_from_rt.keys())

    def test_searching_a_non_existing_movie_through_rt(self):
        movie_title = 'xzcvzdbcvbxz'  # this movie does not exists
        search_response_from_rt = rt_movie_search(movie_title)
        self.assertEqual(search_response_from_rt['movies'], [])

    def test_decoding_retrieved_tmdb_info(self):
        movie_title = 'gremlins'
        search_response_from_tmdb = tmdb_movie_search(movie_title)
        tmdb_structured_movie_info = decode_response_from_tmdb(search_response_from_tmdb)
        with open('D:/diagnostics_files/workable.dat', "a") as f:
            f.write("%s \n" % (tmdb_structured_movie_info, ))
        self.assertEqual(type(tmdb_structured_movie_info), dict)
        self.assertGreater(len(tmdb_structured_movie_info), 0)

    def test_decoding_retrieved_rt_info(self):
        movie_title = 'gremlins'
        search_response_from_rt = rt_movie_search(movie_title)
        rt_structured_movie_info = decode_response_from_rt(search_response_from_rt)
        with open('D:/diagnostics_files/workable.dat', "a") as f:
            f.write("%s \n" % (rt_structured_movie_info, ))
        self.assertEqual(type(rt_structured_movie_info), dict)
        self.assertGreater(len(rt_structured_movie_info), 0)

    def test_merge_responses_from_tmdb_and_rt(self):
        tmdb_gremlins_result = {"(u'Gremlins 2: The New Batch',)":
                                    {'starring_actors': [u'Zach Galligan', u'Phoebe Cates', u'John Glover'],
                                     'number_of_reviews': 10,
                                     'movie_description': u'',
                                     'movie_production_year': u'1990'},
                                "(u'Full Throttle - Gremlin vs. Pacer',)":
                                    {'starring_actors': [],
                                     'number_of_reviews': 0,
                                     'movie_description': u'',
                                     'movie_production_year': u'2006'},
                                "(u'Wiggles, The: Whoo Hoo! Wiggly Gremlins!',)":
                                    {'starring_actors': [u'Anthony Field', u'Greg Page', u'Jeff Fatt'],
                                     'number_of_reviews': 0,
                                     'movie_description': u'',
                                     'movie_production_year': u'2004'},
                                "(u'Gremlins',)":
                                    {'starring_actors': [u'Zach Galligan', u'Hoyt Axton', u'Frances Lee McCain'],
                                     'number_of_reviews': 8,
                                     'movie_description': u'',
                                     'movie_production_year': u'1984'}}

        rt_gremlins_result = {"(u'Gremlins 2: The New Batch',)":
                                  {'starring_actors': [u'Zach Galligan', u'Phoebe Cates', u'John Glover'],
                                   'number_of_reviews': 0,
                                   'movie_description': u'Young sweethearts Billy and Kate move to the Big Apple, land'
                                                        u' jobs in a high-tech office park and soon reunite with the'
                                                        u' friendly and lovable Gizmo. But a series of accidents'
                                                        u' creates a whole new generation of Gremlins. The situation '
                                                        u'worsens when the devilish green creatures invade a top-secret'
                                                        u' laboratory and develop genetically altered powers, making'
                                                        u' them even harder to destroy!',
                                   'movie_production_year': u'1990'},
                              "(u'Gremlins 3',)":
                                  {'starring_actors': [],
                                   'number_of_reviews': 0,
                                   'movie_description': u'Upcoming Gremlins sequel.',
                                   'movie_production_year': 'n/a'},
                              "(u'The Wiggles: Whoo Hoo! Wiggly Gremlins!',)":
                                  {'starring_actors': [u'Emma Watkins', u'Anthony Field', u'Lachlan Gillespie'],
                                   'number_of_reviews': 0,
                                   'movie_description': u'Something strange is happening at Network Wiggles, it appears'
                                                        u' as though the Wiggly Gremlins are playing some funny tricks'
                                                        u' on The Wiggles and their friends. Can you help the "Fab Four'
                                                        u' of Fun" work out who is behind the mischief?',
                                   'movie_production_year': u'2004'},
                              "(u'Spirits of the Air, Gremlins of the Clouds',)":
                                  {'starring_actors': [], 'number_of_reviews': 0,
                                   'movie_description': u'Brother and sister endure alone together in a '
                                                        u'post-apocalyptic Outback until the sudden arrival of a '
                                                        u'stranger.',
                                   'movie_production_year': u'1989'},
                              "(u'Gremlins',)":
                                  {'starring_actors': [u'Zach Galligan', u'Phoebe Cates', u'Hoyt Axton'],
                                   'number_of_reviews': 1,
                                   'movie_description': u'When Billy Peltzer is given a strange but adorable pet named'
                                                        u' Gizmo for Christmas, he inadvertently breaks the three'
                                                        u' important rules of caring for a Mogwai, and unleashes a'
                                                        u' horde of mischievous gremlins on a small town.',
                                   'movie_production_year': u'1984'}}
        self.assertEqual(len(merge_responses_from_tmdb_and_rt(tmdb_gremlins_result, rt_gremlins_result)), 7)

    def test_merge_responses_from_tmdb_and_rt_response_empty(self):
        tmdb_gremlins_result = {"(u'Gremlins 2: The New Batch',)":
                                    {'starring_actors': [u'Zach Galligan', u'Phoebe Cates', u'John Glover'],
                                     'number_of_reviews': 10,
                                     'movie_description': u'',
                                     'movie_production_year': u'1990'},
                                "(u'Full Throttle - Gremlin vs. Pacer',)":
                                    {'starring_actors': [],
                                     'number_of_reviews': 0,
                                     'movie_description': u'',
                                     'movie_production_year': u'2006'},
                                "(u'Wiggles, The: Whoo Hoo! Wiggly Gremlins!',)":
                                    {'starring_actors': [u'Anthony Field', u'Greg Page', u'Jeff Fatt'],
                                     'number_of_reviews': 0,
                                     'movie_description': u'',
                                     'movie_production_year': u'2004'},
                                "(u'Gremlins',)":
                                    {'starring_actors': [u'Zach Galligan', u'Hoyt Axton', u'Frances Lee McCain'],
                                     'number_of_reviews': 8,
                                     'movie_description': u'',
                                     'movie_production_year': u'1984'}}

        rt_gremlins_result = {}
        self.assertEqual(len(merge_responses_from_tmdb_and_rt(tmdb_gremlins_result, rt_gremlins_result)), 4)









