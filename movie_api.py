from flask import Flask
from flask import jsonify, render_template
from movie_api_keys import rotten_tomatoes_key as rt_key
from movie_api_keys import tmdb_key
from flask import Blueprint
import tmdbsimple as tmdb
import rtsimple as rt
import urllib2
import json
import multiprocessing
import jinja2
from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()

app = Flask(__name__)
movie_api_module = Blueprint('movie_api', __name__, url_prefix="/movie_api")

try:
    cpus = multiprocessing.cpu_count()
except NotImplementedError:
    cpus = 2


def tmdb_authentication(tmdb_key):
    tmdb.API_KEY = tmdb_key


def rt_authentication(rt_key):
    rt.API_KEY = rt_key


def tmdb_movie_search(movie_title):
    try:
        if not cache.get(movie_title):
            search = tmdb.Search()
            search_response_from_tmdb = search.movie(query=movie_title)
        else:
            search_response_from_tmdb = 'q cached'
    except (IOError, urllib2.HTTPError) as err:
        return str(err.message) + '. Please try again later'
    return search_response_from_tmdb


def rt_movie_search(movie_title):
    try:
        if not cache.get(movie_title):
            rt_movie = rt.Movies()
            search_response_from_rt = rt_movie.search(q=movie_title)
        else:
            search_response_from_rt = 'q cached'
    except (IOError, urllib2.HTTPError) as err:
        return str(err.message) + '. Please try again later.'
    return search_response_from_rt


def structure_tmdb_movie_info(tmdb_movie_info):
        tmdb_authentication(tmdb_key)
        movie_title = tmdb_movie_info['original_title']
        movie_id = tmdb_movie_info['id']
        movie_description = tmdb_movie_info['overview']
        if len(tmdb_movie_info['release_date']) >= 4:
            movie_production_year = tmdb_movie_info['release_date'][0:4]
        else:
            movie_production_year = 'n/a'
        try:
            review_info = tmdb.Movies(movie_id).reviews()
            credits_info = tmdb.Movies(movie_id).credits()
        except (IOError, urllib2.HTTPError) as err:
            return ((movie_title, ),
                    dict(zip(['movie_description', 'movie_production_year', 'number_of_reviews', 'starring_actors'],
                         [movie_description, movie_production_year, 'n/a', 'n/a'])))
        number_of_reviews = len(review_info['results'])
        list_of_starring_actor_names = [cast['name'] for cast in credits_info['cast']][0:3]
        return ((movie_title, ),
                dict(zip(['movie_description', 'movie_production_year', 'number_of_reviews', 'starring_actors'],
                         [movie_description, movie_production_year, number_of_reviews, list_of_starring_actor_names])))


def decode_response_from_tmdb(search_response_from_tmdb):
    tmdb_movie_dict = {}
    pool = multiprocessing.Pool(processes=cpus)
    tmdb_structured_movie_info = pool.map(structure_tmdb_movie_info, search_response_from_tmdb['results'])
    pool.terminate()
    for tmdb_movie_dict_item in tmdb_structured_movie_info:
        tmdb_movie_dict[str(tmdb_movie_dict_item[0])] = tmdb_movie_dict_item[1]
    return tmdb_movie_dict


def structure_rt_movie_info(rt_movie_info):
        rt_authentication(tmdb_key)
        movie_title = rt_movie_info['title']
        movie_id = rt_movie_info['id']
        movie_description = rt_movie_info['synopsis']
        movie_production_year_list = rt_movie_info['release_dates']
        if 'theater' in movie_production_year_list.keys():
            movie_production_year = movie_production_year_list['theater'][0:4]
        elif 'dvd' in movie_production_year_list.keys():
            movie_production_year = movie_production_year_list['dvd'][0:4]
        else:
            movie_production_year = 'n/a'
        try:
            cast_url = rt_movie_info['links']['cast'] + '?apikey=' + rt_key
            reviews_url = rt_movie_info['links']['reviews'] + '?apikey=' + rt_key
            cast_response = urllib2.urlopen(cast_url)
            reviews_response = urllib2.urlopen(reviews_url)

        except (IOError, urllib2.HTTPError) as err:
            return ((movie_title, ),
                    dict(zip(['movie_description', 'movie_production_year', 'number_of_reviews', 'starring_actors'],
                         [movie_description, movie_production_year, 'n/a', 'n/a'])))
        cast_dict = json.load(cast_response)
        list_of_starring_actor_names = [cast_item['name'] for cast_item in cast_dict['cast']][0:3]
        reviews_dict = json.load(reviews_response)
        number_of_reviews = len(reviews_dict['reviews'])
        return ((movie_title, ),
                dict(zip(['movie_description', 'movie_production_year', 'number_of_reviews', 'starring_actors'],
                         [movie_description, movie_production_year, number_of_reviews, list_of_starring_actor_names])))


def decode_response_from_rt(search_response_from_rt):
    rt_movie_dict = {}
    pool = multiprocessing.Pool(processes=cpus)
    rt_structured_movie_info = pool.map(structure_rt_movie_info, search_response_from_rt['movies'])
    pool.terminate()
    for rt_movie_dict_item in rt_structured_movie_info:
        rt_movie_dict[str(rt_movie_dict_item[0])] = rt_movie_dict_item[1]
    return rt_movie_dict


def select_year_of_production(tmdb_year_of_production,
                              rt_year_of_production):
        try:
            int(rt_year_of_production) # keep this as it includes also dvd release date info
            return rt_year_of_production
        except ValueError:
            try:
                int(tmdb_year_of_production)
                return tmdb_year_of_production
            except ValueError:
                return 'n/a'


def select_starring_actors(tmdb_starring_actors,
                           rt_starring_actors):
    if type(tmdb_starring_actors) == type(rt_starring_actors) == list:
        return max([tmdb_starring_actors, rt_starring_actors], key=len)[0:3]
    elif type(tmdb_starring_actors) == list:
        return tmdb_starring_actors[0:3]
    elif type(rt_starring_actors) == list:
        return rt_starring_actors[0:3]
    else:
        return 'n/a'


def merge_responses_from_tmdb_and_rt(tmdb_structured_movie_info, rt_structured_movie_info):
    tmdb_movie_keys = set(tmdb_structured_movie_info.keys())
    rt_movie_keys = set(rt_structured_movie_info.keys())
    list_of_movie_keys = [tmdb_movie_keys, rt_movie_keys]
    list_of_common_movies_keys = set.intersection(*list_of_movie_keys)
    for common_movie in list_of_common_movies_keys:
        tmdb_structured_movie_info[common_movie]['movie_description'] = \
            rt_structured_movie_info[common_movie]['movie_description'] = \
            max([tmdb_structured_movie_info[common_movie]['movie_description'],
                 rt_structured_movie_info[common_movie]['movie_description']], key=len)
        tmdb_structured_movie_info[common_movie]['number_of_reviews'] = \
            rt_structured_movie_info[common_movie]['number_of_reviews'] = \
            sum(filter(lambda x: isinstance(x, (int, long)),
                       [tmdb_structured_movie_info[common_movie]['number_of_reviews'],
                        rt_structured_movie_info[common_movie]['number_of_reviews'], 0]))
        tmdb_structured_movie_info[common_movie]['movie_production_year'] = \
            rt_structured_movie_info[common_movie]['movie_production_year'] = \
            select_year_of_production(tmdb_structured_movie_info[common_movie]['movie_production_year'],
                                      rt_structured_movie_info[common_movie]['movie_production_year'])
        tmdb_structured_movie_info[common_movie]['starring_actors'] = \
            rt_structured_movie_info[common_movie]['starring_actors'] = \
            select_starring_actors(tmdb_structured_movie_info[common_movie]['starring_actors'],
                                   rt_structured_movie_info[common_movie]['starring_actors'])

    tmdb_structured_movie_info.update(rt_structured_movie_info)
    return tmdb_structured_movie_info


@movie_api_module.route('/', methods=['GET'])
def home():
    return render_template('movie_template.html',
                           title_string="Movie db",
                           movie_info={})


@movie_api_module.route('/now_playing/', methods=['GET'])
def now_playing_movies():
    if not cache.get('now playing'):
        rt_authentication(rt_key)
        tmdb_authentication(tmdb_key)
        try:
            search_response_from_tmdb = tmdb.Movies().now_playing()
        except (IOError, urllib2.HTTPError) as err:
            search_response_from_tmdb = str(err.message) + '. Please try again later'
        try:
            lst = rt.Lists()
            search_response_from_rt = lst.movies_in_theaters()
        except (IOError, urllib2.HTTPError) as err:
            search_response_from_rt = str(err.message) + '. Please try again later'
        if type(search_response_from_tmdb) != str:
            tmdb_structured_movie_info = decode_response_from_tmdb(search_response_from_tmdb)
        else:
            tmdb_structured_movie_info = {}
        if type(search_response_from_rt) != str:
            rt_structured_movie_info = decode_response_from_rt(search_response_from_rt)
        else:
            rt_structured_movie_info = {}
        merged_movie_structured_info = merge_responses_from_tmdb_and_rt(tmdb_structured_movie_info,
                                                                        rt_structured_movie_info)
        if type(search_response_from_rt) != str and type(search_response_from_tmdb) != str:
            cache.set('now playing', merged_movie_structured_info, timeout=694800)
    else:
        merged_movie_structured_info = cache.get('now playing')
    return render_template('movie_template.html',
                           title_string="Movie db",
                           movie_info=merged_movie_structured_info.items())


@movie_api_module.route('/search_movie/<movie_title>', methods=['GET'])
def search_movie(movie_title):
    tmdb_authentication(tmdb_key)
    rt_authentication(rt_key)
    search_response_from_tmdb = tmdb_movie_search(movie_title)
    search_response_from_rt = rt_movie_search(movie_title)
    if search_response_from_tmdb != 'q cached':
        if type(search_response_from_tmdb) != str:
            tmdb_structured_movie_info = decode_response_from_tmdb(search_response_from_tmdb)
        else:
            tmdb_structured_movie_info = {}
        if type(search_response_from_rt) != str:
            rt_structured_movie_info = decode_response_from_rt(search_response_from_rt)
        else:
            rt_structured_movie_info = {}
        merged_movie_structured_info = merge_responses_from_tmdb_and_rt(tmdb_structured_movie_info,
                                                                        rt_structured_movie_info)
        if type(search_response_from_rt) != str and type(search_response_from_tmdb) != str:
            cache.set(movie_title, merged_movie_structured_info, timeout=694800)
    else:
        merged_movie_structured_info = cache.get(movie_title)
    return render_template('movie_template.html',
                           title_string="Movie db",
                           movie_info=merged_movie_structured_info.items())

