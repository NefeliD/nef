from flask import Flask
from movie_api import movie_api_module
import os

app = Flask(__name__)

app.register_blueprint(movie_api_module)

if __name__ == "__main__":
    app.debug = True
    # app.run()
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
