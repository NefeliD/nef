installation guidelines:

python 2.7 (anaconda), editor: pycharm
dependencies:
flask,
tmdbsimple (api wrapper),
rtsimple (api wrapper),
urllib2,
json,
multiprocessing,
werkzeug.contrib.cache.SimpleCache

The application runs locally (127.0.0.1:5000/movie_api/ --> home)

API was developed through flask.
Two endpoints available:
 1. 127.0.0.1:5000/movie_api/search_movie/[name of the movie]
 2. 127.0.0.1:5000/movie_api/now_playing/ 
home: 127.0.0.1:5000/movie_api/
Merge: done
Caching: done
Multiprocessing: done
Robustness: done
Extremely basic interface: done
Thorough testing of the app was not possible due to shortage of time (working full time).

Many thanks for your time and interest in my profile.



